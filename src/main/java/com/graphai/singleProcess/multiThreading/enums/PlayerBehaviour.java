package com.graphai.singleProcess.multiThreading.enums;

public enum PlayerBehaviour {
    Both,
    Initiator,
    Replier
}
