package com.graphai.singleProcess.multiThreading.enums;


public enum Locking {
    Fair,
    Unfair,
    None
}

